# Oniro WG Incubation stage marketing 2022wk03

* Chair: Agustin
* Scriba: Agustin
* Schedule: 2022-01-19

## Participants

Shanda (EF), Agustin B.B.(EF), Donghi (Huawei), Chiara (Huawei), Carlo (Array).  

## Agenda

* FOSDEM - Agustin/Chiara/Shanda/Philippe 10 min
* Events list - Chiara 5 min
* Social media management - Shanda 5 min
* AOB - 10 min

## MoM

### FOSDEM

Presentation

* Press release
* Social media (deferred to the specific point)
* Stands
   * Oniro stand. 
      * Content. What is missing?
      * Planning and logistics.
   * Eclipse Foundation Stand at FOSDEM https://stands.fosdem.org/stands/eclipse-foundation/
* Website
   * #task Add on the landing page a link/button to the mailing list.
   * Check the ticket: https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/issues/18
* #task Add to the TLP the communication channels since we will be adding the link to this page at the Oniro FOSDEM talks.
   * Agustin needs to find out if this is done as part of the provisioning process of the oniro-core project.

Discussion

* Press release: no news
* Videos missing but under control. We will add them.
* Planning and logistics.
   * Donghi coordinates this. Meeting on Monday.
      * Doghni will manage it through the wiki page.
   * Experience at Fosdem 2021 and EclipseCon 2021 with the proposed set up

### Events list (not covered, lack of time)

Presentation

* #link to the events list: https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/tree/main/Events-oniro
* Latest additions
   * Events from NOI, added? Anybody else missing?

Discussion


### Social media management

Presentation

* There was a pending task from last week which was to provide guidelines for this topic. This agenda point covers that task.
* EF have a social media manager. Each WG has their own processes. Some marketing teams have a dedicated person to do this.
* EF uses Hubspot. It is an EF only tool but we can connect it with other tools through plug-ins.
   * Other WG uses something simple so the social admin person can be changed and the control remains in the WG.
   * The social media admin is selected by the Marketing Committee.
      * We can have now if there is consensus and then ratified by the Marketing Committee when it becomes functional.
* There are two scenarios:
   * Campaigns. Just like Donghi did for FOSDEM 2022
   * Reactive approach.
      * The content needs to sent to the EF team or the social admin does it. Also somebody from the marketing team can work as social administrator. We can work on this through plugins. 
* Guidelines from EF #link https://www.eclipse.org/org/documents/social_media_guidelines.php 

Discussion

#task Social administrator. Who should that person be?
#task Plugin and tool to manage the account inside Oniro by the social admin. Which tool should that be. 
   * Agustin: it should be something we all can use, not a corporate tool.
#task create lightweight guidelines to respond to messages.

Social media
* Donghi prepared a .xlsx with social media messages for FOSDEM.
   * Well received

What about images?
* Usually EF provides the cards
* Oniro has designs.
   * We need to be careful about IP.
      * The agency used by Huawei states that everything has to be original. The IP is transferred to Huawei.
      * Discussion about how the content can be transferred to EF
      * #task ask about how we can do this from the EF perspective.

### AOB


* Agustin: news
* New oniro-dev mailing list: https://accounts.eclipse.org/mailing-list/oniro-dev
* New Eclipse Artwork and Logos page: https://www.eclipse.org/org/artwork/ 
* Marketing task force meeting series as part of the Oniro calendar: https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/-/wikis/Meetings


About the community section of the oniroptoject.org website
* Agustin to call the attention on the events we are adding to the community page of the website
   * #link: https://oniroproject.org/community/
   * This needs to be managed and related with the events list.
   * Can we add FOSDEM?
* Is there a plan at this point on what to do related to FOSDEM in the News section?

FAQ
* Proposal to create an infographics with this content. Downloadable so engineers can have it
   * No objections.
* Shanda: The FAQ is done. 


## Relevant links

* Repo: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/tree/main/
* Actions workflow board: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/880
* Actions priority board: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/892 
* Other meetings: #link https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/-/wikis/Meetings 
